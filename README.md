# feeds

opml list of feeds i read

# about

I scraped https://jessimekirk.com/blog/hn_users_links/ (who in turn scraped
HN profile pages) and fetched `<link>` tags pointing to RSS/Atom feeds.

I also scraped the https://hnblogs.substack.com/feed RSS feed for
HN-related blogs and then fetched and looked in those for link tags to
feeds, too.

No endorsement or assertion of quality, express or implied, is the result of
inclusion in this repo.  This is just a scrape.

# license

WTFPL

# author

sneak &lt;[sneak@sneak.berlin](mailto:sneak@sneak.berlin)&gt;
